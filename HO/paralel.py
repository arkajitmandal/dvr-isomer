import os
def sbatch(filename):
    """
    Submit a job and get the job id returned
    """
    submit = os.popen("sbatch  %s"%(filename)).read()
    subId = submit.split()[3].replace("\n","")
    return subId

D=False
ti = 0
tf = 43000
tskip = 100
T = range(ti,tf,tskip)
if (raw_input("Clean RUN?(yes)")=="yes"):
	os.system("rm -rf RUN")
os.mkdir("RUN")
for t in T:
	try:
	 os.mkdir("RUN/t%s"%(t))
	except:
	 pass
	sf = open("submit.sbatch","w+")
        sf.write("#!/bin/bash\n#SBATCH -p exciton -A exciton\n")
        sf.write("#SBATCH -o my_output_%j\n")
	sf.write("#SBATCH --mem-per-cpu=16GB\n")
	sf.write("#SBATCH -t 24:00:00\n")
	sf.write("#SBATCH -N 1\n#SBATCH --ntasks-per-node=1\n")
	sf.write("python dvr.py [%s] %s"%(t,D))
	sf.close()
	os.system("cp ./submit.sbatch ./RUN/t%s/submit.sbatch"%(t))
	os.system("cp ./serial.py ./RUN/t%s/serial.py"%(t))
	os.chdir("./RUN/t%s/"%(t)) 
       	print sbatch("submit.sbatch"), t
	os.chdir("../../") 
