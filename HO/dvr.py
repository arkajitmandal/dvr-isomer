#---- Libraries ---------------------
import sys
import math
import time
import numpy as np
import scipy as sp
from numpy import linalg as LA
#-------------------------------------
tt = sys.argv[1] 
diagonalize = eval(sys.argv[2])
#--------------------------------------
def HO(x):
	m = 1000
	w = 0.1 
	return 0.5*m*(w**2)*x**2

def Hel(r):
    ns = 2
    V = np.zeros((len(r),2,2))
    for ri in range(len(r)):
	V[ri,0,0] = HO(r[ri]) 
	V[ri,1,1] = HO(r[ri]) + 0.1 
    return V

def psi0(R, mass):
    r = np.linspace(*R) 
    #---- electronic state----
    istate = 0  # starts at 0
    nstates = 2
    rn = len(r) 
    #------Shape of wavefunction-------------
    r0 = -0.3
    m =  mass
    w = 0.1 #(2.0*(5.0/27.2114)/m) ** 0.5
    a = m*w
    Phi = np.exp(-0.5 * a * (r-r0)**2)
    N = LA.norm(Phi) 
    #-----------------------------------------
    Psi = np.zeros( nstates * rn, dtype=complex ) 
    Psi[ istate * rn: (istate+1) * rn ] = Phi[:] / N
    return Psi 
#--------------------------------------------------
def Kin(mass,R):
	r = np.linspace(*R)
	nr = len(r) 
	dr = (r[1]-r[0]) 
	K = np.pi/dr
	print dr , ""
	T = np.zeros((nr,nr)) 
	N = nr
	pi = np.pi
	#--- prefactor ------
	A = 1.0/(2.0 * mass)
	Aii = A * (K**2.0/3.0) * (1.0 + 2.0/float(N)**2) 
	Aij = A * (2.0 * K**2.0/float(N)**2) 
	#--------------------
	for i in range(nr):
	 T[i,i] =  Aii 
	 for j in range(i+1,nr):
	  T[i,j] =  Aij * ( ((-1.0)**(float(j)-float(i))) / (math.sin( pi * (float(j)-float(i)) / float(N) ))**2) 
	  T[j,i] =  T[i,j] 
	return T


def showR(R):
	r = np.linspace(*R)  
	V = Hel(r) 
	fob = open("pot.txt","w+") 
	for i in range(R[2]):
		fob.write(str(r[i]) + "\t" + str(V[i,0,0]) + "\t"  +str(V[i,1,1]) + "\n")
	fob.close()


def pop(CDt,nr):
	ns = int(len(CDt)/nr) 	
	density = np.zeros(len(CDt),dtype=complex) 
	for i in range(len(CDt)):
		density[i] = CDt[i] * CDt[i].conjugate()
	rho = np.zeros(ns,dtype=complex) 
	prob = np.zeros((ns,nr),dtype=complex) 
	for i in range(ns):
		rho[i] = np.sum(density[i*nr:(i+1)*nr]).real
		prob[i,:] = density[i*nr:(i+1)*nr]
	return rho, prob

def H(R = [-1.5,1.5,1000], mass = 1 ):
	"""
	R = [rmin,rmax,nsteps] 
	"""
	r = np.linspace(*R) 
	nr = len(r) 
	#--------------------------------
	#--- electronic Hamiltonian -----
	#--- gives ns x ns hel matrix for
	#--- for each value of r --------
        #--------------------------------
	V  = Hel(r)
	ns = len(V[0])
        #--------------------------------
	#---- kinetic energy operator ---
	#---- gives nr x nr T matrix ---- 
	#--------------------------------
	T = Kin(mass, R) 
        #--------------------------------

	N = nr * ns 
	Htot = np.zeros((N,N)) 
	
	for i in range(N):
	 for j in range(N):
	    ri =  i%nr # position index
	    rj =  j%nr # position index
	    ni =  int(i/nr) # electronic state
	    nj =  int(j/nr) # electronic state
	    Htot[i,j] += V[ri,ni,nj]*(ri==rj) + T[ri,rj]*(ni==nj) 
        return Htot

def  Diag(H):
	E,V = LA.eigh(H)
	return E,V

def rotate(V,C):
	return np.matmul(V,C)

#------------------------------------------

if __name__ == "__main__":
 fold = "../../"
 print "---------------------------"
 # Get eigenvalue & eigenvectors
 popfile = open("population.txt","w+")
 psifile = open("psi.txt","w+")
 t0=time.time()
 rmin = -5.0
 rmax = 5.0
 rpoints = 500
 mass = 1000.0 #550.0 * 1836.0
 R = [rmin,rmax,rpoints]
 showR(R)
 if diagonalize:
  Ham = H(R, mass )
  E, V = Diag(Ham) 
  np.savetxt('Eig.txt',E)
  np.savetxt('Vec.txt',V) 
  np.savetxt('Ham.txt',Ham) 
  Ham = 0
 else:
  try: 
   E=np.loadtxt(fold + 'Eig.txt')
   V=np.loadtxt(fold + 'Vec.txt').astype(complex) 
  except:
	
   E=np.loadtxt( 'Eig.txt')
   V=np.loadtxt( 'Vec.txt').astype(complex) 
 Ne=len(E)
 t1=time.time()
 print "Eigenbasis Ready at : " , t1-t0
 C0 = np.zeros(Ne,dtype=complex)
 CD0 = psi0(R, mass)
 for i in range(Ne): 
   for k in range(Ne):
	C0[i]+= CD0[k]*V[k,i]
# print np.matmul(C0.T.conjugate(),C0)

 t2=time.time()
 print "Initial wavefunction", t2-t1
 T = eval(tt)  #range(ti,tf,step) 
 for t in T:
        Ct=np.zeros(Ne,dtype=complex)  
        for i in range(Ne):
         Ct[i] = np.exp(-1j*E[i]*t) * C0[i]
	  
	# diabatic Ct
	CtD = rotate(V,Ct) 
	
        rho, prob =  pop(CtD, rpoints)
	#Rs  =  rs(CtD,ns,nh)    	
	popfile.write(str(t) + " " + " ".join(str(i.real) for i in rho) + "\n" ) 
	for ri in range(rpoints): 
		psifile.write(" ".join(str(i.real) for i in prob[:,ri]) + "\n")  
        
	
	psifile.write("\n\n")
        t3=time.time()
        print t3-t2
        t2=t3
 popfile.close()
 psifile.close()

